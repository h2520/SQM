# What this MR is about:
* This MR is to enhance the existing CI pipeline for Meta-Learning Code.

# Make Sure to check the boxes below before submitting MR:
- [ ] Does the code have descriptive method names?
- [ ] Are the test cases successfully implemented for this code?
- [ ] Does the code implement the requirement for SQM project?
- [ ] Is the test coverage reported for this code?
- [ ] Is the artifact-based test report displayed in the MR user interface?
