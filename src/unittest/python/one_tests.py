#Python 3.6.8 (tags/v3.6.8:3c6b436a57, Dec 24 2018, 00:16:47) [MSC v.1916 64 bit (AMD64)] on win32
#Type "help", "copyright", "credits" or "license()" for more information.
#>>> # coding=utf-8
# Copyright 2019 The Meta-Dataset Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python2, python3
"""Tests for `sampling` module."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl.testing import parameterized
import gin.tf
import sys
sys.path.append( '/mnt/c/Users/Surbhi Sharma/Metadata Project/SQM/src/main/python' )
from meta_dataset.data import sampling
from meta_dataset.data.dataset_spec import DatasetSpecification
from meta_dataset.data.learning_spec import Split
import numpy as np
from six.moves import range
from six.moves import zip
import tensorflow as tf
import unittest

# DatasetSpecification to use in tests
DATASET_SPEC = DatasetSpecification(
    name=None,
    classes_per_split={
        Split.TRAIN: 15,
        Split.VALID: 5,
        Split.TEST: 10
    },
    images_per_class=dict(enumerate([10, 20, 30] * 10)),
    class_names=None,
    path=None,
    file_pattern='{}.tfrecords')

# Define defaults and set Gin configuration for EpisodeDescriptionSampler
MIN_WAYS = 5
MAX_WAYS_UPPER_BOUND = 50
MAX_NUM_QUERY = 10
MAX_SUPPORT_SET_SIZE = 500
MAX_SUPPORT_SIZE_CONTRIB_PER_CLASS = 100
MIN_LOG_WEIGHT = np.log(0.5)
MAX_LOG_WEIGHT = np.log(2)


class SampleNumWaysUniformlyTest(unittest.TestCase):
  """Tests for the `sample_num_ways_uniformly` function."""

  def test_min_ways_respected(self):
    for _ in range(10):
      num_ways = sampling.sample_num_ways_uniformly(
          10, min_ways=MIN_WAYS, max_ways=MAX_WAYS_UPPER_BOUND)
      self.assertGreaterEqual(num_ways, MIN_WAYS)

  def test_num_classes_respected(self):
    num_classes = 10
    for _ in range(10):
      num_ways = sampling.sample_num_ways_uniformly(
          num_classes, min_ways=MIN_WAYS, max_ways=MAX_WAYS_UPPER_BOUND)
      self.assertLessEqual(num_ways, num_classes)

  def test_max_ways_upper_bound_respected(self):
    num_classes = 2 * MAX_WAYS_UPPER_BOUND
    for _ in range(10):
      num_ways = sampling.sample_num_ways_uniformly(
          num_classes, min_ways=MIN_WAYS, max_ways=MAX_WAYS_UPPER_BOUND)
      self.assertLessEqual(num_ways, MAX_WAYS_UPPER_BOUND)


if __name__ == '__main__':
  unittest.main()
