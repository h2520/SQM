#Python 3.6.8 (tags/v3.6.8:3c6b436a57, Dec 24 2018, 00:16:47) [MSC v.1916 64 bit (AMD64)] on win32
#Type "help", "copyright", "credits" or "license()" for more information.
#>>> # coding=utf-8
# Copyright 2019 The Meta-Dataset Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python2, python3
"""Tests for `sampling` module."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl.testing import parameterized
import gin.tf
import sys
sys.path.append( '/mnt/c/Users/Surbhi Sharma/Metadata Project/SQM/src/main/python' )
from meta_dataset.data import sampling
from meta_dataset.data.dataset_spec import DatasetSpecification
from meta_dataset.data.learning_spec import Split
import numpy as np
from six.moves import range
from six.moves import zip
import tensorflow as tf
import unittest

# DatasetSpecification to use in tests
DATASET_SPEC = DatasetSpecification(
    name=None,
    classes_per_split={
        Split.TRAIN: 15,
        Split.VALID: 5,
        Split.TEST: 10
    },
    images_per_class=dict(enumerate([10, 20, 30] * 10)),
    class_names=None,
    path=None,
    file_pattern='{}.tfrecords')

# Define defaults and set Gin configuration for EpisodeDescriptionSampler
MIN_WAYS = 5
MAX_WAYS_UPPER_BOUND = 50
MAX_NUM_QUERY = 10
MAX_SUPPORT_SET_SIZE = 500
MAX_SUPPORT_SIZE_CONTRIB_PER_CLASS = 150
MIN_LOG_WEIGHT = np.log(0.5)
MAX_LOG_WEIGHT = np.log(2)


class SampleSupportSetSizeTest(unittest.TestCase):
  """Tests for the `sample_support_set_size` function."""

  def test_max_support_set_size_respected(self):
    num_remaining_per_class = np.array([MAX_SUPPORT_SET_SIZE] * 10)
    for _ in range(10):
      support_set_size = sampling.sample_support_set_size(
          num_remaining_per_class,
          max_support_size_contrib_per_class=MAX_SUPPORT_SIZE_CONTRIB_PER_CLASS,
          max_support_set_size=MAX_SUPPORT_SET_SIZE)
      self.assertLessEqual(support_set_size, MAX_SUPPORT_SET_SIZE)

  def test_raises_error_max_support_too_small(self):
    num_remaining_per_class = np.array([5] * 10)
    with self.assertRaises(ValueError):
      sampling.sample_support_set_size(
          num_remaining_per_class,
          max_support_size_contrib_per_class=MAX_SUPPORT_SIZE_CONTRIB_PER_CLASS,
          max_support_set_size=len(num_remaining_per_class) - 1)


class SampleNumSupportPerClassTest(unittest.TestCase):
  """Tests for the `sample_num_support_per_class` function."""

  def test_support_set_size_respected(self):
    num_images_per_class = np.array([50, 40, 30, 20])
    num_remaining_per_class = np.array([40, 30, 20, 10])
    support_set_size = 50
    for _ in range(10):
      num_support_per_class = sampling.sample_num_support_per_class(
          num_images_per_class,
          num_remaining_per_class,
          support_set_size,
          min_log_weight=MIN_LOG_WEIGHT,
          max_log_weight=MAX_LOG_WEIGHT)
      self.assertLessEqual(num_support_per_class.sum(), support_set_size)

  def test_at_least_one_example_per_class(self):
    num_images_per_class = np.array([10, 10, 10, 10, 10])
    num_remaining_per_class = np.array([5, 5, 5, 5, 5])
    support_set_size = 5
    for _ in range(10):
      num_support_per_class = sampling.sample_num_support_per_class(
          num_images_per_class,
          num_remaining_per_class,
          support_set_size,
          min_log_weight=MIN_LOG_WEIGHT,
          max_log_weight=MAX_LOG_WEIGHT)
      self.assertTrue((num_support_per_class > 0).any())

  def test_complains_on_too_small_support_set_size(self):
    num_images_per_class = np.array([10, 10, 10, 10, 10])
    num_remaining_per_class = np.array([5, 5, 5, 5, 5])
    support_set_size = 3
    with self.assertRaises(ValueError):
      sampling.sample_num_support_per_class(
          num_images_per_class,
          num_remaining_per_class,
          support_set_size,
          min_log_weight=MIN_LOG_WEIGHT,
          max_log_weight=MAX_LOG_WEIGHT)

  def test_complains_on_zero_remaining(self):
    num_images_per_class = np.array([10, 10, 10, 10, 10])
    num_remaining_per_class = np.array([5, 0, 5, 5, 5])
    support_set_size = 5
    with self.assertRaises(ValueError):
      sampling.sample_num_support_per_class(
          num_images_per_class,
          num_remaining_per_class,
          support_set_size,
          min_log_weight=MIN_LOG_WEIGHT,
          max_log_weight=MAX_LOG_WEIGHT)

if __name__ == '__main__':
  unittest.main()
